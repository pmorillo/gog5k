package gog5k

import (
	"context"
	"fmt"
	"net/http"
)

// VlansService is an interface for interfacing with
// the vlans endpoint of the Grid5000 API
type VlansService interface {
	List(ctx context.Context, site string) ([]Vlan, *Response, error)
	GetNodes(ctx context.Context, site string, id string) ([]VlanNode, *Response, error)
}

// VlansServiceOp handles communication with the vlans retlates methods
// of the Grid5000 API
type VlansServiceOp struct {
	client *Client
}

var _ VlansService = &VlansServiceOp{}

// Vlan respresents a Grid5000 vlan
type Vlan struct {
	UID  string `json:"uid"`
	Type string `json:"type"`
}

type vlansRoot struct {
	Items []Vlan `json:"items"`
}

// VlanNode represent a node with assigned Vlan
type VlanNode struct {
	UID  string `json:"uid"`
	Vlan string `json:"vlan"`
}

type vlansNodesRoot struct {
	Items []VlanNode `json:"items"`
}

// List lists all available vlans
func (s *VlansServiceOp) List(ctx context.Context, site string) ([]Vlan, *Response, error) {
	return s.list(ctx, site)
}

// Helper method for listing vlans
func (s *VlansServiceOp) list(ctx context.Context, site string) ([]Vlan, *Response, error) {
	path := fmt.Sprintf("%s/%s/vlans", siteBasePath, site)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(vlansRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, err
}

// GetNodes list nodes by vlans
func (s *VlansServiceOp) GetNodes(ctx context.Context, site string, id string) ([]VlanNode, *Response, error) {
	path := fmt.Sprintf("%s/%s/vlans/%s/nodes", siteBasePath, site, id)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(vlansNodesRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, nil
}
