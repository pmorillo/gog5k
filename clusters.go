package gog5k

import (
	"context"
	"fmt"
	"net/http"
)

// ClustersService is an interface for interfacing with the clusters
// endpoint of the Grid5000 API
type ClustersService interface {
	List(ctx context.Context, site string) ([]Cluster, *Response, error)
}

// ClustersServiceOp handles communication with cluster related methods
// of the Grid5000 API
type ClustersServiceOp struct {
	client *Client
}

var _ ClustersService = &ClustersServiceOp{}

// Cluster represent a Grid5000 cluster
type Cluster struct {
	UID    string   `json:"uid"`
	Queues []string `json:"queues,omitempty"`
	Model  string   `json:"model,omitempty"`
	Kavlan bool     `json:"kavlan,omitempty"`
}

type clustersRoot struct {
	Items []Cluster `json:"items"`
}

// List lists all available clusters by site
func (s *ClustersServiceOp) List(ctx context.Context, site string) ([]Cluster, *Response, error) {
	return s.list(ctx, site)
}

// Helper method for listing clusters
func (s *ClustersServiceOp) list(ctx context.Context, site string) ([]Cluster, *Response, error) {
	path := fmt.Sprintf("%s/%s/clusters", siteBasePath, site)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(clustersRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, err
}
