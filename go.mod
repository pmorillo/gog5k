module gitlab.inria.fr/pmorillo/gog5k

go 1.13

require (
	github.com/google/go-querystring v1.0.0
	gopkg.in/yaml.v2 v2.3.0
)
