package gog5k

import (
	"context"
	"fmt"
	"net/http"
)

// FirewallService is an interface for interacting with the firewall endpoint
// of the Grid'5000 API
type FirewallService interface {
	Create(ctx context.Context, siteName string, jobID int32, createRequest []*Firewall) (*Response, error)
	Delete(ctx context.Context, siteName string, jobID int32) (*Response, error)
	Get(ctx context.Context, siteName string, jobID int32) ([]Firewall, *Response, error)
}

// FirewallServiceOp handles communication with the firewall related methods
// of the Grid'5000 API
type FirewallServiceOp struct {
	client *Client
}

// Firewall represent a firewal opening
type Firewall struct {
	Dest     []string `json:"addr"`
	Ports    []int    `json:"port,omitempty"`
	Protocol string   `json:"proto,omitempty"`
	Src      []string `json:"src_addr,omitempty"`
}

func (s *FirewallServiceOp) Create(ctx context.Context, siteName string, jobID int32, createRequest []*Firewall) (*Response, error) {
	if createRequest == nil {
		return nil, NewArgError("createRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s/firewall/%d", siteBasePath, siteName, jobID)
	req, err := s.client.NewRequest(ctx, http.MethodPost, path, createRequest)
	if err != nil {
		return nil, err
	}
	resp, err := s.client.Do(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return nil, nil
}

func (s *FirewallServiceOp) Delete(ctx context.Context, siteName string, jobID int32) (*Response, error) {
	path := fmt.Sprintf("%s/%s/firewall/%d", siteBasePath, siteName, jobID)
	req, err := s.client.NewRequest(ctx, http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}
	resp, err := s.client.Do(ctx, req, nil)
	return resp, err
}

func (s *FirewallServiceOp) Get(ctx context.Context, siteName string, jobID int32) ([]Firewall, *Response, error) {
	path := fmt.Sprintf("%s/%s/firewall/%d", siteBasePath, siteName, jobID)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}
	root := make([]Firewall, 0)

	resp, err := s.client.Do(ctx, req, &root)
	return root, resp, err
}
