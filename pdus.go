package gog5k

import (
	"context"
	"fmt"
	"net/http"
)

// PDUsService is an interface for interfacing with
// the pdus endpoint of the Grid5000 API
type PDUsService interface {
	List(ctx context.Context, site string) ([]PDU, *Response, error)
}

// PDUsServiceOp handles communication with the pdus related methods
// of the Grid5000 API
type PDUsServiceOp struct {
	client *Client
}

var _ PDUsService = &PDUsServiceOp{}

// PDU represent a Grid5000 PDU
type PDU struct {
	UID    string            `json:"uid"`
	Vendor string            `json:"vendor"`
	Model  string            `json:"model"`
	IP     string            `json:"ip"`
	Ports  map[string]string `json:"ports"`
}

type pdusRoot struct {
	Items []PDU `json:"items"`
}

// List lists all available PDUs
func (s *PDUsServiceOp) List(ctx context.Context, site string) ([]PDU, *Response, error) {
	return s.list(ctx, site)
}

func (s *PDUsServiceOp) list(ctx context.Context, site string) ([]PDU, *Response, error) {
	path := fmt.Sprintf("%s/%s/pdus", siteBasePath, site)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(pdusRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, nil
}
