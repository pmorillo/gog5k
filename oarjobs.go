package gog5k

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	//OARJobRunningState Running state
	OARJobRunningState = "running"
	//OARJobWaitingState Waiting state
	OARJobWaitingState = "waiting"
	//OARJobTerminatedState Terminated state
	OARJobTerminatedState = "terminated"
	//OARJobErrorState Error state
	OARJobErrorState = "error"
)

// OARJobsService is an interface for interfacing with the OAR jobs endpoint
// of the Grid5000 API
type OARJobsService interface {
	List(ctx context.Context, siteName string, opts *ListOARJobsOptions) ([]OARJob, *Response, error)
	Get(ctx context.Context, siteName string, jobID int32) (*OARJob, *Response, error)
	Create(ctx context.Context, siteName string, createRequest *OARJobCreateRequest) (*OARJob, *Response, error)
	Delete(ctx context.Context, siteName string, jobID int32) (*Response, error)
	WaitForState(ctx context.Context, siteName string, jobID int32, state string) (*OARJob, error)
}

// OARJobsServiceOp handles communication with the site related methods of the
// Grid5000 API.
type OARJobsServiceOp struct {
	client *Client
}

// OARJob represent a Grid5000 OAR job
type OARJob struct {
	State           string                `json:"state"`
	Name            string                `json:"name,omitempty"`
	Queue           string                `json:"queue,omitempty"`
	Properties      string                `json:"propertie,omitempty"`
	Message         string                `json:"message,omitempty"`
	UserUID         string                `json:"user_uid"`
	Events          []OARJobEvent         `json:"events,omitempty"`
	User            string                `json:"user"`
	SubmittedAt     int32                 `json:"submitted_at,omitempty"`
	Types           []string              `json:"types,omitempty"`
	Mode            string                `json:"mode,omitempty"`
	StoppedAt       int32                 `json:"stopped_at,omitempty"`
	StartedAt       int32                 `json:"started_at,omitempty"`
	ScheduledAt     int32                 `json:"scheduled_at,omitempty"`
	Walltime        int32                 `json:"walltime,omitempty"`
	ID              int32                 `json:"uid"`
	Project         string                `json:"project,omitempty"`
	Command         string                `json:"command,omitempty"`
	Directory       string                `json:"directory,omitempty"`
	AssignedNodes   []string              `json:"assigned_nodes,omitempty"`
	ResourcesByType OARJobResourcesByType `json:"resources_by_type,omitempty"`
}

// OARJobEvent respresent a OAR job event
type OARJobEvent struct {
	UID         int32  `json:"uid"`
	Type        string `json:"type"`
	CreatedAt   int32  `json:"created_at"`
	Description string `json:"description"`
}

//OARJobResourcesByType represent a assigned resources by type
type OARJobResourcesByType struct {
	Vlans   []string `json:"vlans"`
	Subnets []string `json:"subnets"`
	Disks   []string `json:"disks"`
}

// OARJobCreateRequest represents a request to create a OAR job.
type OARJobCreateRequest struct {
	Name        string   `json:"name,omitempty"`
	Resources   string   `json:"resources,omitempty"`
	Command     string   `json:"command"`
	Types       []string `json:"types,omitempty"`
	Properties  string   `json:"properties,omitempty"`
	Reservation string   `json:"reservation,omitempty"`
}

type oarJobsRoot struct {
	Items []OARJob `json:"items"`
}

//ListOARJobsOptions struct for options on OAR jobs list
type ListOARJobsOptions struct {
	User  string `url:"user,omitempty"`
	State string `url:"state,omitempty"`
}

// List lists all OAR jobs
func (s *OARJobsServiceOp) List(ctx context.Context, site string, opts *ListOARJobsOptions) ([]OARJob, *Response, error) {
	return s.list(ctx, site, opts)
}

func (s *OARJobsServiceOp) list(ctx context.Context, site string, opts *ListOARJobsOptions) ([]OARJob, *Response, error) {
	path := fmt.Sprintf("%s/%s/jobs", siteBasePath, site)
	path, err := addOptions(path, opts)
	if err != nil {
		return nil, nil, err
	}
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(oarJobsRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, err
}

// Get retrieves a OAR job by ID.
func (s *OARJobsServiceOp) Get(ctx context.Context, siteName string, jobID int32) (*OARJob, *Response, error) {
	return s.get(ctx, siteName, jobID)
}

// Helper method for getting an individual OAR job
func (s *OARJobsServiceOp) get(ctx context.Context, siteName string, jobID interface{}) (*OARJob, *Response, error) {
	path := fmt.Sprintf("%s/%s/jobs/%d", siteBasePath, siteName, jobID)

	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	job := new(OARJob)
	resp, err := s.client.Do(ctx, req, job)
	if err != nil {
		return nil, resp, err
	}

	return job, resp, err
}

// Create Submit a Grid5000 OAR job
func (s *OARJobsServiceOp) Create(ctx context.Context, siteName string, createRequest *OARJobCreateRequest) (*OARJob, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}

	path := fmt.Sprintf("%s/%s/jobs", siteBasePath, siteName)
	req, err := s.client.NewRequest(ctx, http.MethodPost, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	job := new(OARJob)
	resp, err := s.client.Do(ctx, req, job)
	if err != nil {
		return nil, resp, err
	}

	return job, resp, err
}

// Delete a OAR job.
func (s *OARJobsServiceOp) Delete(ctx context.Context, siteName string, jobID int32) (*Response, error) {
	if jobID < 1 {
		return nil, NewArgError("Job ID", "cannot be less than 1")
	}

	path := fmt.Sprintf("%s/%s/jobs/%d", siteBasePath, siteName, jobID)
	fmt.Printf("DELETE : %s", path)
	req, err := s.client.NewRequest(ctx, http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(ctx, req, nil)

	return resp, err
}

// WaitForState get Job state until desired state
func (s *OARJobsServiceOp) WaitForState(ctx context.Context, siteName string, jobID int32, state string) (*OARJob, error) {
	for {
		job, _, err := s.get(ctx, siteName, jobID)
		if err != nil {
			return nil, err
		}
		log.Printf("Job #%d@%s state is %s", jobID, siteName, job.State)
		if job.State == state {
			return job, nil
		}
		time.Sleep(5 * time.Second)
	}
}
