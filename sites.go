package gog5k

import (
	"context"
	"fmt"
	"net/http"
)

const siteBasePath = "stable/grid5000/sites"

// SitesService is an interface for interfacing with the sites
// endpoint of the Grid5000 API
type SitesService interface {
	List(context.Context) ([]Site, *Response, error)
	GetByName(ctx context.Context, siteName string) (*Site, *Response, error)
}

// SitesServiceOp handles communication with the site related methods of the
// Grid5000 API.
type SitesServiceOp struct {
	client *Client
}

var _ SitesService = &SitesServiceOp{}

// Site represents a Grid5000 site
type Site struct {
	UID          string  `json:"uid"`
	Name         string  `json:"name"`
	FrontendIP   string  `json:"frontend_ip,omitempty"`
	EmailContact string  `json:"email_contact,omitempty"`
	Production   bool    `json:"production,omitempty"`
	RenaterIP    string  `json:"renater_ip,omitempty"`
	Longitude    float32 `json:"longitude,omitempty"`
	Latitude     float32 `json:"latitude,omitempty"`
	Description  string  `json:"description,omitempty"`
	Location     string  `json:"location,omitempty"`
}

type sitesRoot struct {
	Items []Site `json:"items"`
}

// List lists all available sites
func (s *SitesServiceOp) List(ctx context.Context) ([]Site, *Response, error) {
	return s.list(ctx)
}

// Helper method for listing sites
func (s *SitesServiceOp) list(ctx context.Context) ([]Site, *Response, error) {
	path := siteBasePath
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(sitesRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, err
}

// GetByName retrieves a site by name.
func (s *SitesServiceOp) GetByName(ctx context.Context, siteName string) (*Site, *Response, error) {
	return s.get(ctx, interface{}(siteName))
}

// Helper method for getting an individual site
func (s *SitesServiceOp) get(ctx context.Context, siteName interface{}) (*Site, *Response, error) {
	path := fmt.Sprintf("%s/%v", siteBasePath, siteName)

	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	site := new(Site)
	resp, err := s.client.Do(ctx, req, site)
	if err != nil {
		return nil, resp, err
	}

	return site, resp, err
}
