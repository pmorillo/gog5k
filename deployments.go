package gog5k

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	// DeploymentTerminatedState Terminated status
	DeploymentTerminatedState = "terminated"
	// DeploymentErrorState Error status
	DeploymentErrorState = "error"
)

// DeploymentsService is an interface for interfacing with the Kadeploy endpoint
// of the Grid5000 API
type DeploymentsService interface {
	List(ctx context.Context, siteName string) ([]Deployment, *Response, error)
	Get(ctx context.Context, siteName string, deploymentID string) (*Deployment, *Response, error)
	Create(ctx context.Context, siteName string, createRequest *DeploymentCreateRequest) (*Deployment, *Response, error)
	WaitForState(ctx context.Context, siteName string, deploymentID string, state string) (*Deployment, error)
}

// DeploymentsServiceOp handles communication with deployments related methods of the
// Grid5000 API.
type DeploymentsServiceOp struct {
	client *Client
}

//var _ DeploymentsService = &DeploymentsServiceOp{}

// Deployment respresent a Grid5000 Kadeploy deployment
type Deployment struct {
	ID          string `json:"uid"`
	Status      string `json:"status"`
	User        string `json:"user_uid"`
	Environment string `json:"environment"`
	Key         string `json:"key,omitempty"`
	CreatedAt   int32  `json:"created_at"`
	UpdatedAt   int32  `json:"updated_at"`
}

// DeploymentCreateRequest represents a request to create a Kadeploy deployment
type DeploymentCreateRequest struct {
	Environment     string   `json:"environment"`
	Key             string   `json:"key,omitempty"`
	Nodes           []string `json:"nodes"`
	Vlan            int16    `json:"vlan,omitempty"`
	PartitionNumber int      `json:"partition_number,omitempty"`
	BlockDevice     string   `json:"block_device,omitempty"`
}

type deploymentsRoot struct {
	Items []Deployment `json:"items"`
}

// List lists all Kadeploy deployments
func (s *DeploymentsServiceOp) List(ctx context.Context, siteName string) ([]Deployment, *Response, error) {
	return s.list(ctx, siteName)
}

func (s *DeploymentsServiceOp) list(ctx context.Context, siteName string) ([]Deployment, *Response, error) {
	path := fmt.Sprintf("%s/%s/deployments", siteBasePath, siteName)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(deploymentsRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}
	return root.Items, resp, err
}

// Get restrieve a Kadeploy deployments by ID
func (s *DeploymentsServiceOp) Get(ctx context.Context, siteName string, deploymentID string) (*Deployment, *Response, error) {
	return s.get(ctx, siteName, deploymentID)
}

// Helper method for getting an individual Kadeploy deployment
func (s *DeploymentsServiceOp) get(ctx context.Context, siteName string, deploymentID string) (*Deployment, *Response, error) {
	path := fmt.Sprintf("%s/%s/deployments/%s", siteBasePath, siteName, deploymentID)

	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	deployment := new(Deployment)
	resp, err := s.client.Do(ctx, req, deployment)
	if err != nil {
		return nil, resp, err
	}

	return deployment, resp, err
}

// Create Submit a Kadeploy deployment
func (s *DeploymentsServiceOp) Create(ctx context.Context, siteName string, createRequest *DeploymentCreateRequest) (*Deployment, *Response, error) {
	if createRequest == nil {
		return nil, nil, NewArgError("createRequest", "cannot be nil")
	}
	fmt.Println("[DEBUG] deploy create")
	path := fmt.Sprintf("%s/%s/deployments", siteBasePath, siteName)
	req, err := s.client.NewRequest(ctx, http.MethodPost, path, createRequest)
	if err != nil {
		return nil, nil, err
	}

	deployment := new(Deployment)
	resp, err := s.client.Do(ctx, req, deployment)
	if err != nil {
		return nil, resp, err
	}

	return deployment, resp, err
}

// WaitForState get deployment state until desired state
func (s *DeploymentsServiceOp) WaitForState(ctx context.Context, siteName string, deploymentID string, state string) (*Deployment, error) {
	for {
		deployment, _, err := s.get(ctx, siteName, deploymentID)
		if err != nil {
			return nil, err
		}
		log.Printf("Deployment #%s@%s state is %s", deploymentID, siteName, deployment.Status)
		if deployment.Status == state {
			return deployment, nil
		}
		if deployment.Status == DeploymentErrorState {
			err = fmt.Errorf("Deployment %s in error state", deploymentID)
			return nil, err
		}
		time.Sleep(10 * time.Second)
	}
}
