package gog5k

import (
	"context"
	"fmt"
	"net/http"
	"strings"
)

// NodesService is an interface for interfacing with the nodes
// endpoint of the Grid5000 API
type NodesService interface {
	Get(ctx context.Context, siteName string, hostName string) (*Node, *Response, error)
	List(ctx context.Context, siteName string, clusterName string) ([]Node, *Response, error)
}

// NodesServiceOp handles communication with nodes related methods
// of the Grid5000 API
type NodesServiceOp struct {
	client *Client
}

var _ NodesService = &NodesServiceOp{}

// Node represent a Grid5000 node
type Node struct {
	UID               string            `json:"uid"`
	NetworkAdapters   []NetworkAdapter  `json:"network_adapters"`
	StorageDevices    []StorageDevice   `json:"storage_devices"`
	Chassis           Chassis           `json:"chassis"`
	Architecture      Architecture      `json:"architecture"`
	BIOS              BIOS              `json:"bios,omitempty"`
	MainMemory        MainMemory        `json:"main_memory"`
	Processor         Processor         `json:"processor"`
	GPUDevices        GPUDevices        `json:"gpu_devices,omitempty"`
	BMCVersion        string            `json:"bmc_version,omitempty"`
	Performance       Performance       `json:"performance,omitempty"`
	OperatingSystem   OperatingSystem   `json:"operating_system,omitempty"`
	Exotic            bool              `json:"exotic,omitempty"`
	SupportedJobTypes SupportedJobTypes `json:"supported_job_types,omitempty"`
}

// NetworkAdapter represent a node network adapter
type NetworkAdapter struct {
	Name            string `json:"name,omitempty"`
	Device          string `json:"device"`
	Mounted         bool   `json:"mounted"`
	Mountable       bool   `json:"mountable"`
	Enabled         bool   `json:"enabled"`
	IP              string `json:"ip,omitempty"`
	IP6             string `json:"ip6,omitempty"`
	Driver          string `json:"driver,omitempty"`
	Vendor          string `json:"vendor,omitempty"`
	Switch          string `json:"switch,omitempty"`
	Bridged         bool   `json:"bridged,omitempty"`
	FirmwareVersion string `json:"firmware_version,omitempty"`
	MAC             string `json:"mac,omitempty"`
	Interface       string `json:"interface,omitempty"`
	Kavlan          bool   `json:"kavlan,omitempty"`
	NetworkAddress  string `json:"network_address,omitempty"`
	SwitchPort      string `json:"switch_port,omitempty"`
	Model           string `json:"model,omitempty"`
	Management      bool   `json:"management,omitempty"`
	Rate            int64  `json:"rate,omitempty"`
}

// StorageDevice represent a node storage device
type StorageDevice struct {
	Device    string `json:"device"`
	Interface string `json:"interface"`
	Vendor    string `json:"vendor,omitempty"`
	// See : https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=11500
	//FirmwareVersion string `json:"firmware_version,omitempty"`
	Size         int64  `json:"size"`
	Storage      string `json:"storage"`
	Model        string `json:"model,omitempty"`
	DeviceByID   string `json:"by_id,omitempty"`
	DeviceByPath string `json:"by_path,omitempty"`
}

// Chassis represent a node chassis
type Chassis struct {
	Name         string `json:"name"`
	Manufacturer string `json:"manufacturer,omitempty"`
	Serial       string `json:"serial,omitempty"`
}

// Architecture represent a node architecture
type Architecture struct {
	PlatformType     string `json:"platform_type"`
	ProcessorsCount  int16  `json:"nb_procs"`
	CoresCount       int16  `json:"nb_cores"`
	ThreadsCount     int16  `json:"nb_threads"`
	CPUCoreNumbering string `json:"cpu_core_numbering"`
}

// BIOS represent a node BIOS information
type BIOS struct {
	Vendor      string `json:"vendor,omitempty"`
	ReleaseDate string `json:"release_date,omitempty"`
}

// MainMemory represent a node memory information
type MainMemory struct {
	RAMSize int64 `json:"ram_size"`
}

// Processor represent a node processor information
type Processor struct {
	MicroArchitecture string `json:"micro_architecture,omitempty"`
	Vendor            string `json:"vendor,omitempty"`
	// See : https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=11500
	//Version           string `json:"version,omitempty"`
	Model            string `json:"model,omitempty"`
	HTCapable        bool   `json:"ht_capable,omitempty"`
	ClockSpeed       int64  `json:"clock_speed,omitempty"`
	InstructionSet   string `json:"instruction_set,omitempty"`
	Microcode        string `json:"microcode,omitempty"`
	OtherDescription string `json:"other_description,omitempty"`
}

// GPUDevice represent GPU device informations
type GPUDevice struct {
	CPUAffinity int    `json:"cpu_affinity,omitempty"`
	Device      string `json:"device,omitempty"`
	Memory      int64  `json:"memory,omitempty"`
	Model       string `json:"model,omitempty"`
	// See : https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=11500
	//PowerDefaultLimit string `json:"power_default_limit,omitempty"`
	VBIOSVersion string `json:"vbios_version,omitempty"`
	Vendor       string `json:"vendor,omitempty"`
}

// GPUDevices represent a map of GPUDevice
type GPUDevices map[string]GPUDevice

// Performance represent a node performances information
type Performance struct {
	NodeFlops int64 `json:"node_flops,omitempty"`
	CoreFlops int64 `json:"core_flops,omitempty"`
}

// OperatingSystem respresent a node operating system information
type OperatingSystem struct {
	TurboBoostEnabled     bool   `json:"turboboost_enabled,omitempty"`
	CStateDriver          string `json:"cstate_driver,omitempty"`
	CStateGovernor        string `json:"cstate_governor,omitempty"`
	PStateDrive           string `json:"pstate_driver,omitempty"`
	PStateGovernor        string `json:"pstate_governor,omitempty"`
	HyperthreadingEnabled bool   `json:"ht_enabled,omitempty"`
}

// SupportedJobTypes respresent supported job types by a node
type SupportedJobTypes struct {
	// See : https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=11500
	//Virtual     string   `json:"virtual"`
	Besteffort  bool     `json:"besteffort"`
	Queues      []string `json:"queues"`
	Deploy      bool     `json:"deploy"`
	MaxWalltime int64    `json:"max_walltime"`
}

type nodesRoot struct {
	Items []Node `json:"items"`
}

// Get retrieves a node
func (s *NodesServiceOp) Get(ctx context.Context, siteName string, hostName string) (*Node, *Response, error) {
	return s.get(ctx, siteName, hostName)
}

func (s *NodesServiceOp) get(ctx context.Context, siteName string, hostName string) (*Node, *Response, error) {
	path := fmt.Sprintf("%s/%s/clusters/%s/nodes/%s", siteBasePath, siteName, clusterName(hostName), hostName)

	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	node := new(Node)
	resp, err := s.client.Do(ctx, req, node)
	if err != nil {
		return nil, resp, err
	}

	return node, resp, nil
}

func clusterName(nodeName string) string {
	return strings.Split(nodeName, "-")[0]
}

// List lists all available clusters by site
func (s *NodesServiceOp) List(ctx context.Context, site string, clusterName string) ([]Node, *Response, error) {
	return s.list(ctx, site, clusterName)
}

// Helper method for listing clusters
func (s *NodesServiceOp) list(ctx context.Context, site string, clusterName string) ([]Node, *Response, error) {
	path := fmt.Sprintf("%s/%s/clusters/%s/nodes", siteBasePath, site, clusterName)
	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	root := new(nodesRoot)
	resp, err := s.client.Do(ctx, req, root)
	if err != nil {
		return nil, resp, err
	}

	return root.Items, resp, err
}
