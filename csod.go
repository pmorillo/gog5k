package gog5k

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

const csodBasePath = "sid"

// CSODService is an interface for interfacing with the ceph
// storage endpoint of the Grid5000 API
type CSODService interface {
	GetAuth(ctx context.Context, siteName string, userName string) (*CSODAuth, *Response, error)
	ListPools(ctx context.Context, siteName string) ([]CSODPool, *Response, error)
	GetPool(ctx context.Context, siteName string, poolname string) (*CSODPool, *Response, error)
	CreatePool(ctx context.Context, siteName string, createRequest *CSODPoolCreateRequest) (*Response, error)
	DeletePool(ctx context.Context, siteName string, poolName string) (*Response, error)
	UpdatePool(ctx context.Context, siteName string, updateRequest *CSODPool) (*Response, error)
}

// CSODServiceOp handles communication with the ceph related methods of the
// Grid5000 API
type CSODServiceOp struct {
	client *Client
}

// CSODAuth represent a Cephx auth
type CSODAuth struct {
	Entity       string               `json:"entity"`
	Key          string               `json:"key"`
	Capabilities CSODAuthCapabilities `json:"caps"`
}

// CSODAuthCapabilities represent a Cephx auth capabilities
type CSODAuthCapabilities struct {
	MON string `json:"mon"`
	OSD string `json:"osd"`
}

// CSODPool represent a Ceph pool
type CSODPool struct {
	Pool           int       `json:"pool"`
	PoolName       string    `json:"pool_name"`
	Size           int       `json:"size"`
	QuotaMaxBytes  int64     `json:"quota_max_bytes"`
	ExpirationDate time.Time `json:"expiration_date"`
}

// CSODPoolCreateRequest represent a request to create a Ceph pool
type CSODPoolCreateRequest struct {
	PoolName       string    `json:"poolName"`
	Size           int       `json:"size"`
	QuotaMaxBytes  int64     `json:"quota"`
	ExpirationDate time.Time `json:"expiration_date"`
}

// GetAuth retrieve a Cephx auth
func (s *CSODServiceOp) GetAuth(ctx context.Context, siteName string, userName string) (*CSODAuth, *Response, error) {
	path := fmt.Sprintf("%s/sites/%s/storage/ceph/auths/%s", csodBasePath, siteName, userName)

	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	auth := new(CSODAuth)
	resp, err := s.client.Do(ctx, req, auth)
	if err != nil {
		return nil, resp, err
	}

	return auth, resp, err
}

// ListPools retrive ceph pool list
func (s *CSODServiceOp) ListPools(ctx context.Context, siteName string) ([]CSODPool, *Response, error) {
	path := fmt.Sprintf("%s/sites/%s/storage/ceph/pools", csodBasePath, siteName)

	req, err := s.client.NewRequest(ctx, http.MethodGet, path, nil)
	if err != nil {
		return nil, nil, err
	}

	pools := new([]CSODPool)
	resp, err := s.client.Do(ctx, req, pools)
	if err != nil {
		return nil, resp, err
	}

	return *pools, resp, err
}

// GetPool retrieve a ceph pool
func (s *CSODServiceOp) GetPool(ctx context.Context, siteName string, poolname string) (*CSODPool, *Response, error) {
	pools, resp, err := s.ListPools(ctx, siteName)
	if err != nil {
		return nil, resp, err
	}
	for _, p := range pools {
		if poolname == p.PoolName {
			return &p, nil, nil
		}
	}
	return nil, resp, err
}

// CreatePool create a ceph pool
func (s *CSODServiceOp) CreatePool(ctx context.Context, siteName string, createRequest *CSODPoolCreateRequest) (*Response, error) {
	if createRequest == nil {
		return nil, NewArgError("createRequest", "cannot be nil")
	}
	path := fmt.Sprintf("%s/sites/%s/storage/ceph/pools", csodBasePath, siteName)
	req, err := s.client.NewRequest(ctx, http.MethodPost, path, createRequest)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, err
}

// UpdatePool update a ceph pool
func (s *CSODServiceOp) UpdatePool(ctx context.Context, siteName string, updateRequest *CSODPool) (*Response, error) {
	if updateRequest == nil {
		return nil, NewArgError("updateRequest", "cannot be nil")
	}
	path := fmt.Sprintf("%s/sites/%s/storage/ceph/pools/%s", csodBasePath, siteName, updateRequest.PoolName)
	req, err := s.client.NewRequest(ctx, http.MethodPut, path, updateRequest)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(ctx, req, nil)
	if err != nil {
		return resp, err
	}

	return resp, err
}

// DeletePool delete a ceph pool
func (s *CSODServiceOp) DeletePool(ctx context.Context, siteName string, poolName string) (*Response, error) {
	path := fmt.Sprintf("%s/sites/%s/storage/ceph/pools/%s", csodBasePath, siteName, poolName)
	req, err := s.client.NewRequest(ctx, http.MethodDelete, path, nil)
	if err != nil {
		return nil, err
	}

	resp, err := s.client.Do(ctx, req, nil)

	return resp, err
}
